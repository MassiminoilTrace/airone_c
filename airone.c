/*
Copyright © 2023 Massimo Gismondi

This file is part of airone_C.

airone_C is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

airone_C is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with airone_C.
If not, see <https://www.gnu.org/licenses/>.
*/

#include "airone.h"
#include <unistd.h>
#include <inttypes.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>

#define BASE_FILENAME_LENGTH 30

// Create the "int" one which is compiler
//  and architecture dependent
static_assert (sizeof (int) == sizeof (int32_t),
	       "The library assumes int equals to i32. Remove _int functions or change architecture");
static_assert (INT_MAX == INT32_MAX,
	       "The library assumes int equals to i32. Remove _int functions or change architecture");
#define TypeInt Typei32

// Enums for supported types
typedef enum
{
  TypeChar,
  TypeFloat,
  TypeDouble,
  Typei8,
  Typeu8,
  Typei16,
  Typeu16,
  Typei32,
  Typeu32,
  Typei64,
  Typeu64,
  TypeString
} SupportedTypes;

union DataValue
{
  char char_data;
  float float_data;
  double double_data;
  // Iso99
  int8_t i8_data;
  uint8_t u8_data;
  int16_t i16_data;
  uint16_t u16_data;
  int32_t i32_data;
  uint32_t u32_data;
  int64_t i64_data;
  uint64_t u64_data;
  char *string_data;
};

// The inner hidden struct
// gets renamed to type AironeDb
struct _airone_db_struct
{
  size_t num_of_fields;
  SupportedTypes *field_types;
  size_t length;
  union DataValue *list;
  FILE *append_log_file;
  char **field_names;

  // how much space has been allocated?
  // 1 capacity = num_of_fields*sizeof(StoredValue)
  size_t capacity;

  // Tmp values when pushing an object field by field
  union DataValue *insertions_tmp_values;
  // -1 = not currently pushing
  // 0 = where the next item will be saved
  // ...
  size_t insertion_tmp_index;

  // Fields can be added only
  // when the db is not initialized yet
  unsigned char initialized;
};

#define _check_initialization(DB, DESIRED_STATE) \
  assert(                                        \
      DESIRED_STATE == DB->initialized)

// Prototypes of
// private functions
char *serialize_value (union DataValue src, SupportedTypes type);
union DataValue deserialize_value (const char *src, SupportedTypes type);

// Semplicemente ogni union nella memoria
// ignorando cosa ci sia dentro.
// Se è una stringa, ne alloca e memorizza una copia
void persist_add_to_memory (AironeDb * db,
			    size_t index, union DataValue argv[]);
void persist_add_to_disk (AironeDb * db,
			  size_t index, union DataValue argv[]);

// Copia il singolo union nella memoria,
// ignorandone il contenuto
void persist_edit_to_memory (AironeDb * db,
			     uintptr_t index,
			     size_t dest_field_number, union DataValue value);
void persist_edit_to_disk (AironeDb * db,
			   uintptr_t index,
			   size_t dest_field_number, union DataValue value);

// Rimuove dalla memoria.
// Se contiene delle stringhe, ne libera la memoria
void persist_delete_to_memory (AironeDb * db, size_t index);
void persist_delete_to_disk (AironeDb * db, size_t index);

void load_base_file (AironeDb * db, char filename[]);
void load_append_file (AironeDb * db, char filename[]);
void dump_base_file (AironeDb * db, char filename[]);

void remove_newline (char *str);

// Implementations
AironeDb *airone_init ()
{
  AironeDb *db;
  db = (AironeDb *) malloc (sizeof (AironeDb));
  db->num_of_fields = 0;
  db->field_types = malloc (sizeof (SupportedTypes));
  db->length = 0;
  db->list = NULL;
  db->field_names = malloc (sizeof (char *));
  ;
  db->append_log_file = NULL;
  db->initialized = 0;
  db->capacity = 0;
  return db;
}

// Utility functions
// Returns db->num_of_fields if not found
uintptr_t _find_field_index_by_name (AironeDb * db,
				     const char *const fieldname)
{
  uintptr_t i = 0;
  for (; i < db->num_of_fields; i++)
  {
    if (strcmp (db->field_names[i], fieldname) == 0)
    {
      return i;
    }
  }
  return i;
}

uintptr_t _find_max_line_length (const char *const filename)
{
  uintptr_t max = 0;
  uintptr_t tmp_max = 0;
  FILE *ptr;
  ptr = fopen (filename, "r");
  if (ptr == NULL)
  {
    return 0;
  }

  const size_t BUFFER_SIZE = 300;
  char buffer[BUFFER_SIZE];
  size_t bytes_read;
  while ((bytes_read = fread (buffer, sizeof (char), BUFFER_SIZE, ptr)) != 0)
  {
    for (int i = 0; i < bytes_read; i++)
    {
      if (buffer[i] == '\n')
      {
	if (tmp_max > max)
	{
	  max = tmp_max;
	}
	tmp_max = 0;
      }
      else
      {
	tmp_max++;
      }
    }
  }
  fclose (ptr);

  // Uno per il \n a fine riga e uno per il \0
  return max + 2;
}

// Checks if the array has to be
// reallocated, made bigger or shrinked
void _reallocate_if_needed (AironeDb * db, unsigned char adding)
{
  const size_t BASE_CAPACITY = 8;
  const uintptr_t db_length = db->length;

  // First initialization
  if (db->capacity == 0)
  {
    db->list =
      malloc (BASE_CAPACITY * sizeof (union DataValue) * db->num_of_fields);
    db->capacity = BASE_CAPACITY;
  }

  uintptr_t new_capacity;
  if (adding)
  {
    if ((db_length <= db->capacity) || db_length < BASE_CAPACITY)
    {
      // Capacity is good enough
      return;
    }

    // Hysteresis mechanism
    uintptr_t tmp_capacity = BASE_CAPACITY;
    while (tmp_capacity < db_length)
    {
      tmp_capacity = tmp_capacity << 1;
    }

    new_capacity = tmp_capacity;
  }
  else
  {
    if (db->capacity == BASE_CAPACITY)
    {				// Already at minimum capacity
      return;
    }

    uintptr_t optimal_capacity = BASE_CAPACITY;
    while (optimal_capacity < db_length)
    {
      optimal_capacity = optimal_capacity << 1;
    }
    if (db->capacity >= (optimal_capacity << 1))
    {
      new_capacity = optimal_capacity << 1;
    }
    else
    {
      return;
    }

    if (new_capacity == db->capacity)
    {
      return;
    }
  }

  db->list = realloc (db->list,
		      new_capacity * sizeof (union DataValue) *
		      db->num_of_fields);
  db->capacity = new_capacity;
}

// ---------------------------
// MACROS
// --------------------------

#define CREATE_DUPLICATE_STRING(DST, SRC)      \
  char *DST = (char *)malloc(strlen(SRC) + 1); \
  strcpy(DST, SRC);

#define FUNCTIONS_GEN_MACRO(C_TYPE_NAME, C_TYPE, AIRONE_TYPE, UNION_FIELD_NAME, USE_CONST)   \
  void airone_init_addfield##_##C_TYPE_NAME(AironeDb *db, char field_name[])           \
  {                                                                               \
    _check_initialization(db, 0);                                                 \
    db->num_of_fields++;                                                          \
    uintptr_t num_f = db->num_of_fields;                                          \
    db->field_types = realloc(db->field_types, sizeof(SupportedTypes) * num_f);   \
    db->field_types[num_f - 1] = AIRONE_TYPE;                                     \
    db->field_names = realloc(db->field_names, sizeof(char *) * num_f);           \
    db->field_names[num_f - 1] = malloc(                                          \
        sizeof(char) * strlen(field_name) + 1);                                   \
    strcpy(db->field_names[num_f - 1], field_name);                               \
  }                                                                               \
                                                                                  \
  void airone_set_##C_TYPE_NAME(                                                  \
      AironeDb *db,                                                               \
      uintptr_t index,                                                            \
      const char * const key,                                                     \
      C_TYPE value)                                                               \
  {                                                                               \
    _check_initialization(db, 1);                                                 \
    assert(index < db->length);                                                   \
    uintptr_t field_index = _find_field_index_by_name(db, key);                   \
    assert(field_index < db->num_of_fields);                                      \
    assert(AIRONE_TYPE == db->field_types[field_index]);                          \
    union DataValue v;                                                            \
    v.UNION_FIELD_NAME = value;                                                   \
    persist_edit_to_memory(                                                       \
        db,                                                                       \
        index,                                                                    \
        field_index,                                                              \
        v);                                                                       \
    persist_edit_to_disk(                                                         \
        db,                                                                       \
        index,                                                                    \
        field_index,                                                              \
        v);                                                                       \
  }                                                                               \
                                                                                  \
  /* ---- INSERT OPERATION ---- */                                                \
  void airone_insert##_##C_TYPE_NAME(AironeDb *db, C_TYPE value)                  \
  {                                                                               \
    _check_initialization(db, 1);                                                 \
    assert(db->insertion_tmp_index < db->num_of_fields);                          \
                                                                                  \
    assert(db->field_types[db->insertion_tmp_index] == AIRONE_TYPE);              \
    db->insertions_tmp_values[db->insertion_tmp_index].UNION_FIELD_NAME = value;  \
    db->insertion_tmp_index++;                                                    \
  }                                                                               \
                                                                                  \
  /* ---- GETTER ---- */                                                          \
  USE_CONST C_TYPE airone_get_##C_TYPE_NAME(AironeDb *db, uintptr_t index, const char * const key) \
  {                                                                               \
    _check_initialization(db, 1);                                                 \
    assert(index < db->length);                                                   \
                                                                                  \
    uintptr_t field_index = _find_field_index_by_name(db, key);                   \
    assert(field_index < db->num_of_fields);                                      \
    assert(AIRONE_TYPE == db->field_types[field_index]);                          \
    return db->list[index * db->num_of_fields + field_index].UNION_FIELD_NAME;    \
  }

FUNCTIONS_GEN_MACRO (char, char, TypeChar, char_data,)
FUNCTIONS_GEN_MACRO (int, int32_t, TypeInt, i32_data,)
FUNCTIONS_GEN_MACRO (float, float, TypeFloat, float_data,)
FUNCTIONS_GEN_MACRO (double, double, TypeDouble, double_data,)
FUNCTIONS_GEN_MACRO (int8_t, int8_t, Typei8, i8_data,)
FUNCTIONS_GEN_MACRO (uint8_t, uint8_t, Typeu8, u8_data,)
FUNCTIONS_GEN_MACRO (int16_t, int16_t, Typei16, i16_data,)
FUNCTIONS_GEN_MACRO (uint16_t, uint16_t, Typeu16, u16_data,)
FUNCTIONS_GEN_MACRO (int32_t, int32_t, Typei32, i32_data,)
FUNCTIONS_GEN_MACRO (uint32_t, uint32_t, Typeu32, u32_data,)
FUNCTIONS_GEN_MACRO (int64_t, int64_t, Typei64, i64_data,)
FUNCTIONS_GEN_MACRO (uint64_t, uint64_t, Typeu64, u64_data,)
FUNCTIONS_GEN_MACRO (string, char *, TypeString, string_data, const)
     void airone_init_end (AironeDb * db, const char *const filename)
{
  _check_initialization (db, 0);
  db->insertions_tmp_values =
    malloc (sizeof (union DataValue) * db->num_of_fields);
  db->insertion_tmp_index = 0;

  assert (strlen (filename) < BASE_FILENAME_LENGTH);

  char full_filename[BASE_FILENAME_LENGTH + 5];
  strcpy (full_filename, filename);
  // pulisco stringa
  for (int i = 0; full_filename[i] != '\0'; i++)
  {
    if (!(isalnum (full_filename[i])))
    {
      full_filename[i] = '_';
    }
  }

  char full_filename_changes[BASE_FILENAME_LENGTH + 5 + 9];
  strcpy (full_filename_changes, full_filename);

  strcat (full_filename, ".csv");
  strcat (full_filename_changes, ".changes.csv");

  load_base_file (db, full_filename);
  load_append_file (db, full_filename_changes);
  dump_base_file (db, full_filename);

  // Leave the append log open
  // and return the struct pointer
  FILE *ptr_append_log = fopen (full_filename_changes, "w");
  db->append_log_file = ptr_append_log;

  db->initialized = 1;
}

void airone_insert_start (AironeDb * db)
{
  _check_initialization (db, 1);
  db->insertion_tmp_index = 0;
}

void airone_insert_at_index (AironeDb * db, uintptr_t index)
{
  _check_initialization (db, 1);
  assert (index <= db->length);
  persist_add_to_memory (db, index, db->insertions_tmp_values);
  persist_add_to_disk (db, index, db->insertions_tmp_values);
}

void airone_insert_at_tail (AironeDb * db)
{
  _check_initialization (db, 1);
  uintptr_t cur_length = db->length;
  airone_insert_at_index (db, cur_length);
}

uintptr_t airone_length (AironeDb * db)
{
  _check_initialization (db, 1);
  return db->length;
}

// REMOVE DATA FUNCTIONS
void airone_remove (AironeDb * db, uintptr_t index)
{
  _check_initialization (db, 1);
  assert (db->length > 0);
  persist_delete_to_memory (db, index);
  persist_delete_to_disk (db, index);
}

// Removes the last element.
// The operation is ignored silently
// if there's nothing to remove
void airone_pop (AironeDb * db)
{
  _check_initialization (db, 1);
  assert (db->length > 0);
  uintptr_t index = db->length - 1;
  persist_delete_to_memory (db, index);
  persist_delete_to_disk (db, index);
  return;
}

// Removes ALL elements from the array
void airone_clear (AironeDb * db)
{
  const uintptr_t LEN = db->length;
  for (uintptr_t i = 0; i < LEN; i++)
  {
    airone_pop (db);
  }
}

void airone_free (AironeDb * db)
{
  _check_initialization (db, 1);
  // Check if there's a string type field
  char string_found = 0;
  for (uintptr_t i = 0; i < db->num_of_fields; i++)
  {
    if (db->field_types[i] == TypeString)
    {
      string_found = 1;
      break;
    }
  }
  // Free all allocated strings of each element
  if (string_found)
  {
    // For every object
    for (uintptr_t index = 0; index < db->length; index++)
    {
      // For every field
      for (uintptr_t i = 0; i < db->num_of_fields; i++)
      {
	uintptr_t element_index = index * db->num_of_fields + i;
	// If it's a string, free its memory
	if (db->field_types[i] == TypeString)
	{
	  free (db->list[element_index].string_data);
	}
      }
    }
  }

  free (db->list);
  if (db->append_log_file != NULL)
  {
    fclose (db->append_log_file);
  }

  free (db->field_types);

  for (uintptr_t i = 0; i < db->num_of_fields; i++)
  {
    free (db->field_names[i]);
  }
  free (db->field_names);

  free (db->insertions_tmp_values);

  free (db);
}

// Internal functions

// SERIALIZE
#define SERIALIZE_GENERATOR(FORMAT, AIRONE_TYPE, UNION_FIELD_NAME, C_TYPE_NAME) \
  /* Serialize_per_tipo */                                                      \
  char *serialize_value_##C_TYPE_NAME(C_TYPE_NAME src)                          \
  {                                                                             \
    char *dst = malloc(sizeof(char) * 30);                                      \
    sprintf(dst, FORMAT, src);                                                  \
    return dst;                                                                 \
  }

SERIALIZE_GENERATOR ("%c", TypeChar, char_data, char)
SERIALIZE_GENERATOR ("%f", TypeFloat, float_data, float)
SERIALIZE_GENERATOR ("%lf", TypeDouble, double_data, double)
SERIALIZE_GENERATOR ("%" PRId8, Typei8, i8_data, int8_t)
SERIALIZE_GENERATOR ("%" PRIu8, Typeu8, u8_data, uint8_t)
SERIALIZE_GENERATOR ("%" PRId16, Typei16, i16_data, int16_t)
SERIALIZE_GENERATOR ("%" PRIu16, Typeu16, u16_data, uint16_t)
SERIALIZE_GENERATOR ("%" PRId32, Typei32, i32_data, int32_t)
SERIALIZE_GENERATOR ("%" PRIu32, Typeu32, u32_data, uint32_t)
SERIALIZE_GENERATOR ("%" PRId64, Typei64, i64_data, int64_t)
SERIALIZE_GENERATOR ("%" PRIu64, Typeu64, u64_data, uint64_t)
     char *serialize_value_string (const char *src)
{
  const size_t STRLEN_ORIGINAL = strlen (src);
  uintptr_t n_of_tab_newline = 0;
  for (uintptr_t i = 0; i < STRLEN_ORIGINAL; i++)
  {
    if (src[i] == '\n' || src[i] == '\t')
    {
      n_of_tab_newline++;
    }
  }

  char *dst = malloc (sizeof (char) * strlen (src) + 1 + n_of_tab_newline);
  uintptr_t new_str_index = 0;
  const char *string_to_ser = src;
  for (uintptr_t i = 0; i < STRLEN_ORIGINAL; i++)
  {
    if (string_to_ser[i] == '\n')
    {
      dst[new_str_index] = '\\';
      new_str_index += 1;
      dst[new_str_index] = 'n';
    }
    else if (string_to_ser[i] == '\t')
    {
      dst[new_str_index] = '\\';
      new_str_index += 1;
      dst[new_str_index] = 't';
    }
    else
    {
      dst[new_str_index] = string_to_ser[i];
    }
    new_str_index++;
  }
  dst[new_str_index] = '\0';
  return dst;
}

char *serialize_value (union DataValue src, SupportedTypes type)
{
  switch (type)
  {
  case TypeChar:
    return serialize_value_char (src.char_data);
  case TypeFloat:
    return serialize_value_float (src.float_data);
  case TypeDouble:
    return serialize_value_double (src.double_data);
  case Typei8:
    return serialize_value_int8_t (src.i8_data);
  case Typeu8:
    return serialize_value_uint8_t (src.u8_data);
  case Typei16:
    return serialize_value_int16_t (src.i16_data);
  case Typeu16:
    return serialize_value_uint16_t (src.u16_data);
  case Typei32:
    return serialize_value_int32_t (src.i32_data);
  case Typeu32:
    return serialize_value_uint32_t (src.u32_data);
  case Typei64:
    return serialize_value_int64_t (src.i64_data);
  case Typeu64:
    return serialize_value_uint64_t (src.u64_data);
  case TypeString:
    return serialize_value_string (src.string_data);
  default:
    fprintf (stderr, "Could not serialize type %d}", type);
    exit (1);
    break;
  }
}

// DESERIALIZE
#define DESERIALIZE_GENERATOR(C_TYPE, FORMAT, AIRONE_TYPE, C_TYPE_NAME) \
  C_TYPE deserialize_value_##C_TYPE(const char *src)                    \
  {                                                                     \
    C_TYPE out;                                                         \
    sscanf(src, FORMAT, &out);                                          \
    return out;                                                         \
  }

DESERIALIZE_GENERATOR (char, "%c", TypeChar, char_data)
DESERIALIZE_GENERATOR (int, "%d", TypeInt, int_data)
DESERIALIZE_GENERATOR (float, "%f", TypeFloat, float_data)
DESERIALIZE_GENERATOR (double, "%lf", TypeDouble, double_data)
// ISO C99
DESERIALIZE_GENERATOR (int8_t, "%" SCNd8, Typei8, i8_data)
DESERIALIZE_GENERATOR (uint8_t, "%" SCNu8, Typeu8, u8_data)
DESERIALIZE_GENERATOR (int16_t, "%" SCNd16, Typei16, i16_data)
DESERIALIZE_GENERATOR (uint16_t, "%" SCNu16, Typeu16, u16_data)
DESERIALIZE_GENERATOR (int32_t, "%" SCNd32, Typei32, i32_data)
DESERIALIZE_GENERATOR (uint32_t, "%" SCNu32, Typeu32, u32_data)
DESERIALIZE_GENERATOR (int64_t, "%" SCNd64, Typei64, i64_data)
DESERIALIZE_GENERATOR (uint64_t, "%" SCNu64, Typeu64, u64_data)
     char *deserialize_value_string (const char *src)
{
  // Compute how many values will be swapped
  const size_t SRC_LEN = strlen (src);
  uintptr_t n_of_tab_newline = 0;
  for (uintptr_t i = 0; i < SRC_LEN; i++)
  {
    if (src[i] == '\\' && (src[i + 1] == 'n' || src[i + 1] == 't'))
    {
      n_of_tab_newline++;
    }
  }

  char *dst_tmp =
    malloc (sizeof (char) * (strlen (src) - n_of_tab_newline + 1));
  uintptr_t dst_i = 0;
  for (uintptr_t i = 0; i < SRC_LEN; i++)
  {
    if (src[i] == '\\' && src[i + 1] == 'n')
    {
      dst_tmp[dst_i] = '\n';
      i++;
    }
    else if (src[i] == '\\' && src[i + 1] == 't')
    {
      dst_tmp[dst_i] = '\t';
      i++;
    }
    else
    {
      dst_tmp[dst_i] = src[i];
    }
    dst_i++;
  }
  dst_tmp[dst_i] = '\0';

  return dst_tmp;
}

union DataValue deserialize_value (const char *src, SupportedTypes type)
{
  union DataValue ret;
  switch (type)
  {
  case TypeChar:
    ret.char_data = deserialize_value_char (src);
    break;
  case TypeFloat:
    ret.float_data = deserialize_value_float (src);
    break;
  case TypeDouble:
    ret.double_data = deserialize_value_double (src);
    break;
  case Typei8:
    ret.i8_data = deserialize_value_int8_t (src);
    break;
  case Typeu8:
    ret.u8_data = deserialize_value_uint8_t (src);
    break;
  case Typei16:
    ret.i16_data = deserialize_value_int16_t (src);
    break;
  case Typeu16:
    ret.u16_data = deserialize_value_uint16_t (src);
    break;
  case Typei32:
    ret.i32_data = deserialize_value_int32_t (src);
    break;
  case Typeu32:
    ret.u32_data = deserialize_value_uint32_t (src);
    break;
  case Typei64:
    ret.i64_data = deserialize_value_int64_t (src);
    break;
  case Typeu64:
    ret.u64_data = deserialize_value_uint64_t (src);
    break;
  case TypeString:
    ret.string_data = deserialize_value_string (src);
    break;
  default:
    fprintf (stderr, "Could not deserialize type %d}", type);
    exit (1);
    break;
  }
  return ret;
}

void persist_add_to_memory (AironeDb * db,
			    uintptr_t index, union DataValue argv[])
{
  db->length += 1;
  _reallocate_if_needed (db, 1);
  memmove (&(db->list[(index + 1) * db->num_of_fields]),	// dest
	   &(db->list[index * db->num_of_fields]),	// src
	   sizeof (union DataValue) * db->num_of_fields * (db->length - 1 - index)	// num of bytes
    );
  for (uintptr_t i = 0; i < db->num_of_fields; i++)
  {
    uintptr_t l_index = (index) * db->num_of_fields;
    uintptr_t element_index = l_index + i;
    if (db->field_types[i] == TypeString)
    {
      CREATE_DUPLICATE_STRING (dst, argv[i].string_data);
      db->list[element_index].string_data = dst;
    }
    else
    {
      db->list[element_index] = argv[i];
    }
  }
}

void persist_add_to_disk (AironeDb * db,
			  uintptr_t index, union DataValue argv[])
{
  fprintf (db->append_log_file, "A\t%ld\t", index);

  for (int i = 0; i < db->num_of_fields; i++)
  {
    char *out_string_value = serialize_value (argv[i], db->field_types[i]);
    fprintf (db->append_log_file, "%s", out_string_value);
    free (out_string_value);

    if (i < db->num_of_fields - 1)
    {
      // Tutte le iterazioni fino alla penultima
      fprintf (db->append_log_file, "\t");
    }
    else
    {
      // Ultima iterazione
      fprintf (db->append_log_file, "\n");
    }
  }
}

void persist_edit_to_memory (AironeDb * db,
			     uintptr_t index,
			     size_t dest_field_number, union DataValue value)
{

  // Add better error handling
  assert (dest_field_number < db->num_of_fields);

  const uintptr_t item_index = index * db->num_of_fields + dest_field_number;
  if (db->field_types[dest_field_number] == TypeString)
  {
    char *internal_str = db->list[item_index].string_data;
    internal_str = realloc (internal_str, strlen (value.string_data) + 1);
    strcpy (internal_str, value.string_data);
    db->list[item_index].string_data = internal_str;
  }
  else
  {
    db->list[item_index] = value;
  }
}

void persist_edit_to_disk (AironeDb * db,
			   uintptr_t index,
			   size_t dest_field_number, union DataValue value)
{
  char *out_string_value =
    serialize_value (value, db->field_types[dest_field_number]);
  fprintf (db->append_log_file, "E\t%ld\t%s\t%s\n", index,
	   db->field_names[dest_field_number], out_string_value);
  free (out_string_value);
}

void persist_delete_to_memory (AironeDb * db, uintptr_t index)
{
  for (uintptr_t i = 0; i < db->num_of_fields; i++)
  {
    uintptr_t element_index = index * db->num_of_fields + i;
    if (db->field_types[i] == TypeString)
    {
      free (db->list[element_index].string_data);
    }
  }

  memmove (&(db->list[index * db->num_of_fields]),
	   &(db->list[(index + 1) * db->num_of_fields]),
	   sizeof (union DataValue) * db->num_of_fields * (db->length -
							   index - 1));
  db->length--;
  // Truncate items out of bounds
  _reallocate_if_needed (db, 0);
}

void persist_delete_to_disk (AironeDb * db, uintptr_t index)
{
  fprintf (db->append_log_file, "D\t%ld\n", index);
}

void load_base_file (AironeDb * db, char filename[])
{
  if (access (filename, F_OK) != 0)
  {
    // no file
    return;
  }

  FILE *base_file = fopen (filename, "r");

  const uintptr_t BUFFER_SIZE = _find_max_line_length (filename);
  const int BUFFER_SIZE_INT = (int) BUFFER_SIZE;
  if (BUFFER_SIZE_INT < 0)
  {
    printf ("line too big, casting to int would malloc negative numbers");
  }

  char *buffer = malloc (sizeof (char) * (uintptr_t) BUFFER_SIZE_INT);
  char *buffer_orig_ptr = buffer;
  // Verify column names
  {
    char *column_line = fgets (buffer, BUFFER_SIZE_INT, base_file);
    if (column_line == NULL)
    {
      printf ("No column found in base CSV\n");
      abort ();
    }
    // Remove ending newline character
    remove_newline (column_line);

    char *token;
    // Initializing headers
    for (int i = 0; i < db->num_of_fields; i++)
    {
      token = strsep (&column_line, "\t");

      if (token == NULL)
      {
	fprintf (stderr, "Not enough columns in CSV\n");
	abort ();
      }
      if (strcmp (token, db->field_names[i]) != 0)
      {
	fprintf (stderr, "Column name mismatch between initialization and CSV first line\n\
          Found %s, expected %s ",
		 token, db->field_names[i]);
	abort ();
      }
    }
    // Calling a last one to check there are no more left
    if (strsep (&column_line, "\t") != NULL)
    {
      fprintf (stderr, "Too many columns in the CSV file\n");
      abort ();
    }
    buffer = buffer_orig_ptr;
  }

  // Read and parse the actual values
  while (fgets (buffer, BUFFER_SIZE_INT, base_file) != NULL)
  {
    remove_newline (buffer);

    for (int i = 0; i < db->num_of_fields; i++)
    {
      // Read each value
      char *current_col_value_str;
      current_col_value_str = strsep (&buffer, "\t");

      //
      if (current_col_value_str == NULL)
      {
	fprintf (stderr, "Not enough columns in CSV data\n");
	abort ();
      }
      db->insertions_tmp_values[i] = deserialize_value (current_col_value_str,
							db->field_types[i]);

      // Parse and add it to the memory
    }
    // After parsing each column,
    // ensure there is no data left in buffer
    if (strsep (&buffer, "\t") != NULL)
    {
      fprintf (stderr, "Too many columns data in provided CSV\n");
      abort ();
    }

    persist_add_to_memory (db, db->length, db->insertions_tmp_values);
    for (uintptr_t i = 0; i < db->num_of_fields; i++)
    {
      if (db->field_types[i] == TypeString)
      {
	// Data persisted and copied,
	// delete the buffer
	free (db->insertions_tmp_values[i].string_data);
      }
    }
    buffer = buffer_orig_ptr;
  }

  // if (
  //   db->list[db->length-1].type==TypeString
  //   &&
  //   strlen(
  //     db->list[db->length-1].data.string_data
  //   ) == 0
  //   )
  {
    // Files always have an ending \n
    // It will be parsed as an empty string value
    // The last one is not an actual value,
    // remove it
    // persist_delete_to_memory(db, db->length-1);
  }

  fclose (base_file);
  free (buffer);
}

void load_append_file (AironeDb * db, char filename[])
{
  // Add incremental changes to the in-memory data

  if (access (filename, F_OK) != 0)
  {
    // no file
    return;
  }

  FILE *f_ptr = fopen (filename, "r");

  const uintptr_t BUFFER_SIZE = _find_max_line_length (filename);
  const int BUFFER_SIZE_INT = (int) BUFFER_SIZE;
  if (BUFFER_SIZE_INT < 0)
  {
    printf ("line too big, casting to int would malloc negative numbers");
  }

  char *buffer = malloc (sizeof (char) * BUFFER_SIZE);
  char *buffer_orig_ptr = buffer;
  uintptr_t line_number = 0;
  while (fgets (buffer, BUFFER_SIZE_INT, f_ptr) != NULL)
  {
    // Read one line at a time
    remove_newline (buffer);
    if (strlen (buffer) == 0)
    {
      // Last line only contained a \n
      break;
    }

    char *op_str;
    op_str = strsep (&buffer, "\t");
    char op = op_str[0];
    if (op == 'A')
    {
      // Add and insert operation
      char *new_value = strsep (&buffer, "\t");
      if (new_value == NULL)
      {
	fprintf (stderr, "Invalid add operation");
	abort ();
      }
      uintptr_t index;
      sscanf (new_value, "%" SCNuPTR, &index);

      for (int i = 0; i < db->num_of_fields; i++)
      {
	new_value = strsep (&buffer, "\t");
	if (new_value == NULL)
	{
	  fprintf (stderr, "Invalid add operation");
	  abort ();
	}

	db->insertions_tmp_values[i] = deserialize_value (new_value,
							  db->field_types[i]);
      }

      persist_add_to_memory (db, index, db->insertions_tmp_values);
      for (int i = 0; i < db->num_of_fields; i++)
      {
	if (db->field_types[i] == TypeString)
	{
	  free (db->insertions_tmp_values[i].string_data);
	}
      }
    }
    else if (op == 'E')
    {
      // Edit a field operation
      char *index_str = strsep (&buffer, "\t");
      if (index_str == NULL)
      {
	fprintf (stderr, "Invalid edit operation");
	abort ();
      }
      uintptr_t index;
      sscanf (index_str, "%" SCNuPTR, &index);

      char *fieldname = strsep (&buffer, "\t");
      if (fieldname == NULL)
      {
	fprintf (stderr, "Invalid edit operation");
	abort ();
      }

      char *new_value_str = strsep (&buffer, "\t");
      if (new_value_str == NULL)
      {
	fprintf (stderr, "Invalid edit operation");
	abort ();
      }

      uintptr_t field_index = _find_field_index_by_name (db, fieldname);
      assert (field_index < db->num_of_fields);

      union DataValue new_value = deserialize_value (new_value_str,
						     db->field_types
						     [field_index]);
      persist_edit_to_memory (db, index, field_index, new_value);
      if (db->field_types[field_index] == TypeString)
      {
	free (new_value.string_data);
      }
    }
    else if (op == 'D')
    {
      // Delete an entry operation
      char *index_str = strsep (&buffer, "\t");
      if (index_str == NULL)
      {
	fprintf (stderr, "Invalid delete operation at line %ld", line_number);
	abort ();
      }
      uintptr_t index;
      sscanf (index_str, "%" SCNuPTR, &index);

      persist_delete_to_memory (db, index);
    }
    else
    {
      fprintf (stderr, "Operation %c not supported at line %ld", op,
	       line_number);
    }
    line_number++;
    buffer = buffer_orig_ptr;
  }
  fclose (f_ptr);
  free (buffer);
}

void dump_base_file (AironeDb * db, char filename[])
{
  FILE *f_ptr = fopen (filename, "w");
  // Write header
  for (int i = 0; i < db->num_of_fields; i++)
  {
    fprintf (f_ptr, "%s", db->field_names[i]);
    if (i < db->num_of_fields - 1)
    {
      fprintf (f_ptr, "\t");
    }
    else
    {
      fprintf (f_ptr, "\n");
    }
  }
  // Write data
  for (uintptr_t i = 0; i < db->length; i++)
  {
    for (uintptr_t c = 0; c < db->num_of_fields; c++)
    {
      char *out_string_value =
	serialize_value (db->list[i * db->num_of_fields + c],
			 db->field_types[c]);
      fprintf (f_ptr, "%s", out_string_value);
      free (out_string_value);
      if (c < db->num_of_fields - 1)
      {
	fprintf (f_ptr, "\t");
      }
      else
      {
	fprintf (f_ptr, "\n");
      }
    }
  }
  fclose (f_ptr);
}

void remove_newline (char *str)
{
  size_t len = strlen (str);
  if (str[len - 1] == '\n')
  {
    str[len - 1] = '\0';
  }
}
