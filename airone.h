/*
Copyright © 2023 Massimo Gismondi

This file is part of airone_C.

airone_C is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

airone_C is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with airone_C.
If not, see <https://www.gnu.org/licenses/>. 
*/

#ifndef _AIRONE_H
#define _AIRONE_H
#endif

#ifdef _AIRONE_H

// Stripped down version of inttypes.h
#include <stdint.h>

typedef struct _airone_db_struct AironeDb;

// -----------------------------
// INITIALIZATION PHASE
// -----------------------------

// Creates a new AironeDb with
// the given struct field names.
AironeDb *airone_init ();

// Add a field
void airone_init_addfield_char (AironeDb * db, char field_name[]);
void airone_init_addfield_int (AironeDb * db, char field_name[]);
void airone_init_addfield_float (AironeDb * db, char field_name[]);
void airone_init_addfield_double (AironeDb * db, char field_name[]);
void airone_init_addfield_int8_t (AironeDb * db, char field_name[]);
void airone_init_addfield_uint8_t (AironeDb * db, char field_name[]);
void airone_init_addfield_int16_t (AironeDb * db, char field_name[]);
void airone_init_addfield_uint16_t (AironeDb * db, char field_name[]);
void airone_init_addfield_int32_t (AironeDb * db, char field_name[]);
void airone_init_addfield_uint32_t (AironeDb * db, char field_name[]);
void airone_init_addfield_int64_t (AironeDb * db, char field_name[]);
void airone_init_addfield_uint64_t (AironeDb * db, char field_name[]);
void airone_init_addfield_string (AironeDb * db, char field_name[]);

// void airone_init_addfield_string(char field_name[], AironeDb* db);

// After adding fields, this function completes the initalization
// and loads existing data, if any.
// Pass the filename, without extension, where
// data will be loaded from and saved to.
void airone_init_end (AironeDb * db, const char filename[]);




// ---------------------------------------
// Functions to access, read and edit data.
// Use only after initalization is completed.
// ---------------------------------------

// Access the element at provided "index"
// and set its "key" field at "value"
#define airone_set(DB, INDEX, KEY, VALUE) \
_Generic( \
  (VALUE), \
  char: airone_set_char,\
  float: airone_set_float,\
  double: airone_set_double,\
  int8_t: airone_set_int8_t,\
  uint8_t: airone_set_uint8_t,\
  int16_t: airone_set_int16_t,\
  uint16_t: airone_set_uint16_t,\
  int32_t: airone_set_int32_t,/*Alias for int*/\
  uint32_t: airone_set_uint32_t,\
  int64_t: airone_set_int64_t,\
  uint64_t: airone_set_uint64_t\
)(DB, INDEX, KEY, VALUE)

void airone_set_char (AironeDb * db, uintptr_t index, const char *key,
		      char value);
void airone_set_int (AironeDb * db, uintptr_t index, const char *key,
		     int value);
void airone_set_float (AironeDb * db, uintptr_t index, const char *key,
		       float value);
void airone_set_double (AironeDb * db, uintptr_t index, const char *key,
			double value);
//Iso C99
void airone_set_int8_t (AironeDb * db, uintptr_t index, const char *key,
			int8_t value);
void airone_set_uint8_t (AironeDb * db, uintptr_t index, const char *key,
			 uint8_t value);
void airone_set_int16_t (AironeDb * db, uintptr_t index, const char *key,
			 int16_t value);
void airone_set_uint16_t (AironeDb * db, uintptr_t index, const char *key,
			  uint16_t value);
void airone_set_int32_t (AironeDb * db, uintptr_t index, const char *key,
			 int32_t value);
void airone_set_uint32_t (AironeDb * db, uintptr_t index, const char *key,
			  uint32_t value);
void airone_set_int64_t (AironeDb * db, uintptr_t index, const char *key,
			 int64_t value);
void airone_set_uint64_t (AironeDb * db, uintptr_t index, const char *key,
			  uint64_t value);
void airone_set_string (AironeDb * db, uintptr_t index, const char *key,
			char *value);


/*
  // Insert a new element at the end of the array.
  // Start the push operation
  void airone_push_start(AironeDb* db);

  // Then push fields one at a time, in the correct order
  void airone_push_value_char(AironeDb* db, char value);
  void airone_push_value_int(AironeDb* db, char value);

  // Afterwards, commit finish the push operation and save the newly added values
  void airone_push_finish(AironeDb* db);
*/



void airone_insert_start (AironeDb * db);

void airone_insert_char (AironeDb * db, char value);
void airone_insert_int (AironeDb * db, int value);
void airone_insert_float (AironeDb * db, float value);
void airone_insert_int8_t (AironeDb * db, int8_t value);
void airone_insert_uint8_t (AironeDb * db, uint8_t value);
void airone_insert_int16_t (AironeDb * db, int16_t value);
void airone_insert_uint16_t (AironeDb * db, uint16_t value);
void airone_insert_int32_t (AironeDb * db, int32_t value);
void airone_insert_uint32_t (AironeDb * db, uint32_t value);
void airone_insert_int64_t (AironeDb * db, int64_t value);
void airone_insert_uint64_t (AironeDb * db, uint64_t value);
void airone_insert_string (AironeDb * db, char *value);


void airone_insert_at_index (AironeDb * db, uintptr_t index);
void airone_insert_at_tail (AironeDb * db);

uintptr_t airone_length (AironeDb * db);


// Get a value, given the index of the struct and its field name
char airone_get_char (AironeDb * db, uintptr_t index, const char *key);
int airone_get_int (AironeDb * db, uintptr_t index, const char *key);
float airone_get_float (AironeDb * db, uintptr_t index, const char *key);
double airone_get_double (AironeDb * db, uintptr_t index, const char *key);
int8_t airone_get_int8_t (AironeDb * db, uintptr_t index, const char *key);
uint8_t airone_get_uint8_t (AironeDb * db, uintptr_t index, const char *key);
int16_t airone_get_int16_t (AironeDb * db, uintptr_t index, const char *key);
uint16_t airone_get_uint16_t (AironeDb * db, uintptr_t index,
			      const char *key);
int32_t airone_get_int32_t (AironeDb * db, uintptr_t index, const char *key);
uint32_t airone_get_uint32_t (AironeDb * db, uintptr_t index,
			      const char *key);
int64_t airone_get_int64_t (AironeDb * db, uintptr_t index, const char *key);
uint64_t airone_get_uint64_t (AironeDb * db, uintptr_t index,
			      const char *key);
const char *airone_get_string (AironeDb * db, uintptr_t index,
			       const char *key);

// Removes the element at the provided index
void airone_remove (AironeDb * db, uintptr_t index);

// Removes the last element
void airone_pop (AironeDb * db);

// Removes ALL elements from the array
void airone_clear (AironeDb * db);

// Cleans up resources,
// frees memory and 
// closes the append-only file stream
void airone_free (AironeDb * db);

#endif
