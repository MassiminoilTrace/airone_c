#include "airone.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

const char *filename = "test_empty_string";

int main ()
{
  AironeDb *db = airone_init ();
  airone_init_addfield_string (db, "ccc");
  airone_init_end (db, filename);
  assert (airone_length (db) == 0);
  airone_clear (db);

  airone_insert_start (db);
  airone_insert_string (db, "");
  airone_insert_at_tail (db);

  airone_insert_start (db);
  airone_insert_string (db, "");
  airone_insert_at_tail (db);
  assert (airone_length (db) == 2);

  airone_set_string (db, 0, "ccc", "");
  assert (airone_length (db) == 2);

  airone_free (db);

  // Reload to check if it crashes
  db = airone_init ();
  airone_init_addfield_string (db, "ccc");
  airone_init_end (db, filename);
  assert (airone_length (db) == 2);
  airone_free (db);


  // Will load from base file
  db = airone_init ();
  airone_init_addfield_string (db, "ccc");
  airone_init_end (db, filename);
  assert (airone_length (db) == 2);
  airone_free (db);


  // Test of clear
  db = airone_init ();
  airone_init_addfield_string (db, "ccc");
  airone_init_end (db, filename);
  assert (airone_length (db) == 2);
  airone_clear (db);
  assert (airone_length (db) == 0);
  airone_free (db);


  // Reload and check it has been cleared
  db = airone_init ();
  airone_init_addfield_string (db, "ccc");
  airone_init_end (db, filename);
  assert (airone_length (db) == 0);
  airone_free (db);

  return 0;
}
