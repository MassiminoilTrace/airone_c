/*
Copyright © 2023 Massimo Gismondi

This file is part of airone_C.

airone_C is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

airone_C is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with airone_C.
If not, see <https://www.gnu.org/licenses/>. 
*/

#include "airone.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

const char *filename = "test_string_only";

int main ()
{
  AironeDb *db = airone_init ();
  airone_init_addfield_string (db, "ccc");
  airone_init_end (db, filename);

  airone_insert_start (db);
  airone_insert_string (db, "test_string1!");
  airone_insert_at_tail (db);

  airone_insert_start (db);
  airone_insert_string (db, "foo bar");
  airone_insert_at_tail (db);

  // airone_insert_start(db);
  // airone_insert_string(db, "Option\t \n!\n");
  // airone_insert_at_tail(db);

  airone_remove (db, 0);
  airone_free (db);


  db = airone_init ();
  airone_init_addfield_string (db, "ccc");
  airone_init_end (db, filename);

  assert (airone_length (db) == 1);
  assert (strcmp ("foo bar", airone_get_string (db, 0, "ccc")) == 0);

  airone_clear (db);
  airone_free (db);
}
