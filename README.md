# Airone C

A pure C port of [airone](https://gitlab.com/MassiminoilTrace/airone) Rust library.

Imagine having a list of structs in memory, but with an additional benefit: when some data changes, the modification is automatically persisted to file in a fast way.

Mainly an exercise for myself to master manual memory allocation in C. By the way, it can be useful in pure C programs, as a simple database library.


## Differences between C and Rust

Airone C uses a slightly different approach compared to the rust version.
While the Rust version generates new methods based on the field names in
your structs, the C version requires the programmer to push one field at
a time in the correct order or specify the exact field name.  This choice
was intentional as the C preprocessor is too weak to handle such complex code generation.

This design choice moves the responsibility from the library maintainer
to the end programmer. You are responsible. Be sure to  always pass the
correct number of values and to avoid mistyping field names. These mistakes
can not be detected by the C compiler and segfaults will occur.

If you desire a middle ground, integrating airone in a C project without losing the powerful Rust compiler checks, have a look at [this template](https://gitlab.com/MassiminoilTrace/airone_c_template) on how to compile airone Rust version as a shared library which will be accessible from C.


# How to use

See the provided `example.c` file.

Run `make man` to read the library manpage inside a terminal.

A part of the documentation is written in this readme too as a quick introduction.

## Import
First, include the header of the library
```c
#include "airone.h"
```



All the functions which include a type in the signature name do support all these types:

( Standard C types )
- char
- int
- float
- double
- string (a char* internally). Functions with "string" in the name will receive a char* as an argument

The library also supports ISO C99 fixed size types, you may want to include them with
```c
#include <stdint.h>
```

Then, these types will be available:
- int8_t
- uint8_t
- int16_t
- uint16_t
- int32_t
- uint32_t
- int64_t
- uint64_t



## Initialization and freeing
Allocate a new, not initialized, object.
```c
AironeDb* db = airone_init();
```

Specify its fields, one at a time.
The field type is extracted automatically from the function name.
```c
airone_init_addfield_int8_t(db, "fieldname_a");
airone_init_addfield_int(db, "foo");
airone_init_addfield_string(db, "bar");
```

Finish the initialization.
Data will be saved in two files generated from the filename you provide. Do NOT add extension at the end of the filename.
```c
airone_init_end(db, "filename_to_load_save");
```

Remember to free the resources when you want to close airone

```c
airone_free(db);
```

## Editing data
Access the element at the provided "index"
and set its "key" field at "value"
```c
airone_set_int(db, 1, "foo", 56);
airone_set_string(db, 0, "bar", "My_new_string");
```
## Inserting data
As in C we can't generate a single push method that take into account the desired input types, inserting data is a multi-step process.

First, tell airone you want to insert a new element.
```c
airone_insert_start(db);
```

Then, specify each field of the new element, checking the correct type. Functions have to be called in the correct field order.
```c
airone_insert_int8_t(db, 14);
airone_insert_int(db, 15);
airone_insert_string(db, "RMS");
```

At the end, specify if this element has to be inserted at the end of the array or at a specific index.
```c
// Insert at the end of the array
airone_insert_at_tail(db);
// ** OR **
// insert at at the given index
airone_insert_at_index(db, 0);
```

## Reading a value
You can get and read a field value, given the index of the element and its field name. Change the function name according to the correct field type.
```c
int airone_get_int(AironeDb* db, uintptr_t index, const char* key);
```

## Array length
You can know how many elements are contained in the array
with
```c
airone_length(db);
```

## Deleting data
Removes the element at the provided index
```c
airone_remove(db, index);
```

Removes the last element
```c
airone_pop(AironeDb* db);
```

Removes all the elements:
```c
airone_clear(AironeDb* db);
```

## Free
Free the memory by using
```c
void airone_free(AironeDb* db);
```

# Copyright

This is **NOT** public domain, make sure to respect the license terms.
You can find the license text in the COPYING file.

Copyright © 2023 Massimo Gismondi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
