/*
Copyright © 2023 Massimo Gismondi

This file is part of airone_C.

airone_C is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

airone_C is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with airone_C.
If not, see <https://www.gnu.org/licenses/>. 
*/

#include "airone.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

int main ()
{
  AironeDb *db = airone_init ();
  airone_init_addfield_int8_t (db, "aaa");
  airone_init_addfield_int (db, "bbb");
  airone_init_addfield_string (db, "ccc");
  airone_init_end (db, "ciao");

  for (int i = 0; i < 5; i++)
  {
    char *field_v = "test_string";
    airone_insert_start (db);
    airone_insert_int8_t (db, 5);
    airone_insert_int (db, 1555);
    airone_insert_string (db, field_v);
    airone_insert_at_index (db, 0);
  }

  airone_insert_start (db);
  airone_insert_int8_t (db, 14);
  airone_insert_int (db, 15);
  airone_insert_string (db, "Op\tpi\no\n");
  airone_insert_at_tail (db);
  printf ("%d\n", airone_get_int (db, 0, "bbb"));

  // Test with empty string
  {
    airone_insert_start (db);
    airone_insert_int8_t (db, 5);
    airone_insert_int (db, 1555);
    airone_insert_string (db, "zzz");
    // Insert it at index 5
    airone_insert_at_index (db, 5);

    printf ("Printing empty string: %s\n\n",
	    airone_get_string (db, 5, "ccc"));
  }

  size_t len = airone_length (db);
  for (size_t i = 0; i < len; i++)
  {
    printf ("%d, %d, %s\n",
	    airone_get_int8_t (db, i, "aaa"),
	    airone_get_int (db, i, "bbb"), airone_get_string (db, i, "ccc"));
  }

  airone_set_int (db, 1, "bbb", 56);

  airone_set_string (db, 0, "ccc", "BELANDI!");
  for (size_t i = 0; i < len; i++)
  {
    printf ("%d, %d, %s\n",
	    airone_get_int8_t (db, i, "aaa"),
	    airone_get_int (db, i, "bbb"), airone_get_string (db, i, "ccc"));
  }
  printf ("%s\n", airone_get_string (db, 0, "ccc"));

  airone_remove (db, 1);
  //airone_clear(db);
  airone_free (db);
}
