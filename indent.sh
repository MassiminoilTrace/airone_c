#!/bin/sh
# Use GNU Indent to format code style

for file in ./*.h
do
    indent -bl -bli0 -npsl "$file"
done

for file in ./*.c
do
    indent -bl -bli0 -npsl "$file"
done
