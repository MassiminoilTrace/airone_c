all: build run

.PHONY: clean

FLAG_COMPILAZIONE_DEBUG := -g3 -ggdb3 -O0 -Wpedantic -Werror -Wall -Wconversion

build:
	gcc -o example.out -O3 -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=3 -Werror -Wall -Wconversion airone.c example.c

run: build
	./example.out

build_test: clean
	
	gcc -o test_full.out $(FLAG_COMPILAZIONE_DEBUG) airone.c test_full.c
	gcc -o test_no_string.out $(FLAG_COMPILAZIONE_DEBUG) airone.c test_no_string.c
	gcc -o test_string_only.out $(FLAG_COMPILAZIONE_DEBUG) airone.c test_string_only.c
	gcc -o test_empty_string.out $(FLAG_COMPILAZIONE_DEBUG) airone.c test_empty_string.c

test: build_test
	valgrind -q --leak-check=full ./test_no_string.out
	valgrind -q --leak-check=full ./test_string_only.out
	valgrind -q --leak-check=full ./test_empty_string.out
	
	valgrind -q --leak-check=full ./test_full.out
	valgrind -q --leak-check=full ./test_full.out
	valgrind -q --leak-check=full ./test_full.out
	valgrind -q --leak-check=full ./test_full.out
	valgrind -q --leak-check=full ./test_full.out

man:
	groff -mandoc -Tutf8 airone.3 | less -R

bsdman:
	mandoc airone.3 | less

clean:
	rm -f test_no_string.csv test_no_string.changes.csv
	rm -f test_string_only.csv test_string_only.changes.csv
	rm -f test_empty_string.csv test_empty_string.changes.csv
	rm -f test_full.csv test_full.changes.csv
	rm -f *.out

.PHONY=all build run memcheck man bsdman
